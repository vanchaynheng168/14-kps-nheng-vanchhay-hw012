
// import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Menu from './components/Menu';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './components/Home'
import NoteFound from './components/NoteFound';
import View from './components/View';
import Video from './components/Video';
import Acount from './components/Acount';

import React, { Component } from 'react'
import Index from './components/Index';
import Auth from './components/Auth';
import Wellcome from './components/Wellcome';
import ProtectedRout from './components/ProtectedRoute'

export default class App extends Component {
  constructor(){
    super();
    this.state = {
      data: [{
        id : 1,
        title : 'love is blind',
        description : 'Love Is Blind is a dating reality television series, produced by Kinetic Content .',
        image : 'https://cdn.britannica.com/30/182830-050-96F2ED76/Chris-Evans-title-character-Joe-Johnston-Captain.jpg'
      },
      {
        id : 2,
        title : 'Study more hard',
        description : 'Love Is Blind is a dating reality television series, produced by Kinetic Content .',
        image : 'https://cdn.britannica.com/30/182830-050-96F2ED76/Chris-Evans-title-character-Joe-Johnston-Captain.jpg'
      },
      {
        id : 3,
        title : 'Game make happy',
        description : 'Love Is Blind is a dating reality television series, produced by Kinetic Content .',
        image : 'https://cdn.britannica.com/30/182830-050-96F2ED76/Chris-Evans-title-character-Joe-Johnston-Captain.jpg'
      },
      {
        id : 4,
        title : 'Game make happy',
        description : 'Love Is Blind is a dating reality television series, produced by Kinetic Content .',
        image : 'https://cdn.britannica.com/30/182830-050-96F2ED76/Chris-Evans-title-character-Joe-Johnston-Captain.jpg'
      },
      {
        id : 5,
        title : 'Game make happy',
        description : 'Love Is Blind is a dating reality television series, produced by Kinetic Content .',
        image : 'https://cdn.britannica.com/30/182830-050-96F2ED76/Chris-Evans-title-character-Joe-Johnston-Captain.jpg'
      },
      {
        id : 6,
        title : 'Game make happy',
        description : 'Love Is Blind is a dating reality television series, produced by Kinetic Content .',
        image : 'https://cdn.britannica.com/30/182830-050-96F2ED76/Chris-Evans-title-character-Joe-Johnston-Captain.jpg'
      },
      {
        id : 7,
        title : 'Game make happy',
        description : 'Love Is Blind is a dating reality television series, produced by Kinetic Content .',
        image : 'https://cdn.britannica.com/30/182830-050-96F2ED76/Chris-Evans-title-character-Joe-Johnston-Captain.jpg'
      },
      {
        id : 8,
        title : 'Game make happy',
        description : 'Love Is Blind is a dating reality television series, produced by Kinetic Content .',
        image : 'https://cdn.britannica.com/30/182830-050-96F2ED76/Chris-Evans-title-character-Joe-Johnston-Captain.jpg'
      }]
    }
  }
  render() {
    return (
      <div className="App">
      <Router>
      <Menu />
        <Switch>
          <Route path='/' exact component={Index}/>
          <Route path='/Home' render = {()=><Home data={this.state.data}/>}/>
          <Route path='/Video' component={Video}/>
          <Route path='/Acount' exact component={Acount}/>
          <Route path='/Auth' exact component={Auth}/>
          <ProtectedRout exact path='/Wellcome'  component={Wellcome}/>
          <Route path='/Posts/:id' render = {(props)=><View {...props} data = {this.state.data}/>}/>
          <Route path='*' component={NoteFound}/>
        </Switch>
      </Router> 
    </div>
    )
  }
}
