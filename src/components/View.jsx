import React from 'react'

function View({match, data}) {
    var viewDetail = data.find((d)=>d.id == match.params.id)

    return (
        <div>
            <h1>This is content from post {viewDetail.id}</h1>
        </div>
    )
}
export default View
