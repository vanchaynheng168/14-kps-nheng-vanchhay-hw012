import auth from './Auths'
import React from 'react'

function Wellcome(props) {
  return (
    <div>
        <h1>Wellcome</h1>
        <button onClick = {() =>{
          auth.logout(() => {
            props.history.push("/Auth");
          }
          );
        }
      }
        >
          Logout
        </button>
    </div>
  )
}

export default  Wellcome;
