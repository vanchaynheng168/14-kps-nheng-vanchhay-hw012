import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import auth from './Auths'

function ProtectedRoute({component: Component, ...rest}) {
    //for control route 
    return (
        <Route
        {...rest}
        render = {props => {
            if(auth.isAuth()){
                console.log('if true');
                return <Component {...props} />;
            }
            else{
                console.log("else");
                return <Redirect to = {
                    {
                        pathname: "/Auth",
                        state: {
                            from: props.location
                        }
                    }
                 } />
            }  
        }}
        />
    
    );
};

export default ProtectedRoute
