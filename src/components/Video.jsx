import React from 'react'
import {Container} from 'react-bootstrap'
import { Route } from 'react-router-dom';
import { Switch, Link} from 'react-router-dom'


const Animations = () =>(
        <div>
            <h2>Animation Category</h2>
            <ul>
                <li><Link to = {"/Video/Animations/Action"}> Action </Link></li>
                <li><Link to = {"/Video/Animations/Rmantic"}> Rmantic </Link></li>
                <li><Link to = {"/Video/Animations/Comedy"}> Comedy </Link></li>
            </ul>
            <Switch>
                <Route path="/Video/Animations/:AnimationsName" component={DetailAnimation}/>
                <Route path = "/Video/Animations/" exact render={() => <h1>Please select a topic.</h1>}/>
            </Switch>
        </div>
)
const DetailAnimation = (props) => (
    <div>
        <h2>{props.match.params.AnimationsName}</h2>
    </div>
)
const Movies = () =>(
    <div>
        <h2>Movie Category</h2>
        <ul>
            <li><Link to = {"/Video/Movies/Adventure"}> Adventure </Link></li>
            <li><Link to = {"/Video/Movies/Comedy"}> Comedy </Link></li>
            <li><Link to = {"/Video/Movies/Crime"}> Crime </Link></li>
            <li><Link to = {"/Video/Movies/Documentary"}> Documentary </Link></li>
        </ul>
        <Switch>
            <Route path="/Video/Movies/:MoviesName" component={DetailMovies}/>
            <Route path = "/Video/Movies/" exact render={() => <h1>Please select a topic.</h1>}/>
        </Switch>
    </div>
)
const DetailMovies = (props) => (
    <div>
        <h2>{props.match.params.MoviesName}</h2>
    </div>
)
function Video(props) {
    return (
        <Container style = {{textAlign : 'left'}} className = "mt-3">
            <ul >
                <li><Link to = "/Video/Animations">Animation</Link></li>
                <li><Link to = "/Video/Movies">Movies</Link></li>
            </ul>
            <Switch>
                <Route exact path = "/Video/" render={() => <h1>Please select a topic.</h1>}/>
                <Route path = {"/Video/Animations"} component={Animations}/>
                <Route path = {"/Video/Movies"} component={Movies}/>
            </Switch> 
            {/* <h3>Please choos a topic123</h3> */}
        </Container>
        
    )
}

export default Video
