import React from 'react'
import {Container} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import queryString from 'query-string'

function Acount(props) {
    let pv = queryString.parse(props.location.search)
    if(pv.name === undefined);
    return (
        <Container style = {{textAlign : 'left'}} className = "mt-3">
            <h3>Acount</h3>
            <ul>
                <li><Link to = {`/Acount?name=Netflix`}>Netflix</Link></li>
                <li><Link to = {`/Acount?name=Zillow Group`}>Zillow Group</Link></li>
                <li><Link to = {`/Acount?name=Yahoo`}>Yahoo</Link></li>
                <li><Link to = {`/Acount?name=Modus Create`}>Modus Create</Link></li>
            </ul>
            <h3>The {pv.name===undefined?"no name":"name"} in the query string is {pv.name===undefined?"":`"${pv.name}"`}</h3>
        </Container>
    )
}
export default Acount
