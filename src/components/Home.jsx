import React from 'react'
import { Container,Card, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'
export default function Home(props) {
    // console.log(props.data)
   let view = props.data.map((data)=>
        <Card style={{ width: '18rem' }} className = "mt-3 mr-5" key={data.id}>
        <Card.Img variant="top" src={data.image} />
        <Card.Body>
            <Card.Title>Card Title</Card.Title>
            <Card.Text>
            Some quick example text to build on the card title and make up the bulk of
            the card's content.
            </Card.Text>
            <Card.Text>
                <Link to = {`/Posts/${data.id}`}>see more</Link>
            </Card.Text>
            </Card.Body>
        </Card>  
    )
    return (
        <Container>
            <Row>
                {view}
            </Row>
            
        </Container>
    )
}
