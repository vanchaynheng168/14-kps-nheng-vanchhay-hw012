import React from 'react'
import {Container,Row} from 'react-bootstrap'
import auth from './Auths'

function Auth(props) {
  return (
    <Container>
            <Row>
            <form className="mt-5">
                <input type="text" placeholder="User Name" className="mr-2"></input>
                <input type="password" placeholder="Password" className="mr-2"></input>
                <button onClick={() => {
                  auth.login(() =>{
                    props.history.push("/Wellcome");
                  })
                }}>Submit</button>
            </form>
            </Row>
            </Container>
  )
}
export default Auth
