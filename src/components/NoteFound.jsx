import React from 'react'

function NoteFound() {
    return (
        <div>
            <h1>Error not found 404.</h1>
        </div>
    )
}

export default NoteFound
